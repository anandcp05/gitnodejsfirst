var testnew = require('./handler.js');


module.exports  = [
     {
        method: 'GET',
        path: '/index',
        handler: function (request, reply) {
            testnew.process(function(response){
            	reply.view('index',{"response":response})
            });
    }
}];