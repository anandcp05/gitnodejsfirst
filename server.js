'use strict';

const Hapi = require('hapi');
const server = new Hapi.Server();

//var serviceNappUrls = require('./serviceNappUrls.js');
var routes = require('./routes.js');


    server.connection({
        port: process.env.PORT || 3002,
        routes: {
            cors: {
              origin: ['*'],
              additionalHeaders: ['cache-control', 'x-requested-with']
            }
          }
    });

    server.register(require('vision'), (err) => {    
        if (err) {
            print(err);
            throw err;
        }
        server.views({
              engines: { dust: require('hapi-dust') },
              path: __dirname + '/view'  
        })
        server.start((err) => {
            
    
            
            if (err) {
                throw err;
            }
            
            console.log('Ppl Profile Server started and running well...at: ', server.info.uri);
            
        });
    });

server.route(routes);